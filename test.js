/* Home Control Assistant Cloud Trigger API
 * http://www.hca.tech.com
 * 
 * March 2017
 *
 * This sample file shows how to use the HCA Cloud Trigger module.
 * You will need to get your token by following the instructions provided.
 */

// Include the HCA-Cloud module
var hcacloud = require('./index.js');

// Set your token here
var token = "<put your token here>";

// Specify if you want to use debug for console output
var debug = true;

/* Enter your trigger data here
 * 
 * For this example, we will include two parameters: action & status 
 */
var trigger_data = {action: "cold", status: "52"};

// Create the HCA-Cloud instance
hcacloud.create(token, debug);

// Send trigger
hcacloud.trigger(trigger_data, function(err) {
	if (err) {
		console.log("Got error: " + err);
	} else {
		console.log("Ok, all good");
	}
});

