// Generate a v4 UUID (random) 
const uuid = require('uuid');
var request = require('request');

var debug = false;
var token = "";

function dbg(data) {
	if (debug) {
		console.log(data);
	}
}

exports.trigger = function(data, cb) {
	var datapost = data;
	
	dbg("Sending Trigger to HCA");
  
	if (token == "") {
		dbg("Token not set, call create first.");
		cb("Token not set, call create first.");
	}
  
	var RequestID = GenerateRequestId();
	dbg("Request ID is: " + RequestID);
	
	datapost.requestId = RequestID;
	
	var url = "https://trigger.hcatech.com/api";
	dbg("Using URL: " + url);
	
	var auth = "Bearer " + token;
  
	dbg('Trigger payload: ' + JSON.stringify(datapost));
	
	dbg('Posting trigger');
	
    request.post({
   	  headers: {'content-type' : 'application/json', 'authorization' : auth},
   	  url:     url,
   	  json: 	true,
   	  body:    datapost
   	},
   		function (error, response, body) {
   			if (!error && response.statusCode == 200) {
   				dbg("Trigger response received");
   				cb(null);
   			} else {
   				dbg(error);
   				cb(error);
   			}
     }
   );  
}

exports.create = function (tkn, dbgenabled) {
  debug = dbgenabled;
  dbg("Creating HCA instance");
  dbg("Debug enabled");
  
  token = tkn;
  dbg("Token set to: " + token);
}

function GenerateRequestId() {
	dbg("Generating Request ID");
	
	return uuid();
}
